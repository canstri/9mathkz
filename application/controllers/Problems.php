<?php
class Problems extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->model('news_model');
                $this->load->helper('url_helper');
        }

        public function index(){
            $data['problems'] = $this->news_model->get_problems();
            $data['title'] = 'News archive';

            $this->load->view('templates/header', $data);
            $this->load->view('problems/index', $data);
            $this->load->view('templates/footer');
        }

        public function view($slug = NULL){
                $data['news_item'] = $this->news_model->get_problems($slug);

                if (empty($data['news_item']))
                {
                        $this->load->view('templates/header', $data);
                }

                $data['title'] = $data['news_item']['title'];

                $this->load->view('templates/header', $data);
                $this->load->view('problems/view', $data);
                $this->load->view('templates/footer');
        }
        public function create()
        {
            $this->load->helper('form');
            $this->load->library('form_validation');

            $data['title'] = 'Create a news item';

            $this->form_validation->set_rules('title', 'Title', 'required');
            $this->form_validation->set_rules('text', 'Text', 'required');

            if ($this->form_validation->run() === FALSE)
            {
                $this->load->view('templates/header', $data);
                $this->load->view('problems/create');
                $this->load->view('templates/footer');

            }
            else
            {
                $this->news_model->set_problems();
                $this->load->view('problems/success');
            }
        }




}