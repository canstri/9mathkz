<?php
class Pages extends CI_Controller {

    public function view($page = 'body'){
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter

        $this->load->view('templates/header', $data);
        $this->load->view('pages/'.$page, $data);
        $this->load->view('templates/footer', $data);
	}
/* -----------------------------------------------------------------------------------------
    public function __construct(){
        parent:: __construct();
        $this->load->library('session');
    }


    public function setSessions(){
        $this->session->userdata('item');
        $name = $this->session->userdata('name');
    }

/*-----------------------------------------------------------------------------------------*/
	public function body(){
                $this->load->view('templates/header');
                $this->load->view('pages/body');
                $this->load->view('templates/footer');
        }
        public function about(){
                

                $this->load->view('templates/header');
                $this->load->view('pages/about');
                $this->load->view('templates/footer');
        }

        public function contacts(){
                

                $this->load->view('templates/header');
                $this->load->view('pages/contacts');
                $this->load->view('templates/footer');
        }
        public function community(){
                

                $this->load->view('templates/header');
                $this->load->view('pages/community');
                $this->load->view('templates/footer');
        }

}