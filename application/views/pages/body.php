  
            

            

            
                <div class="land">
                                <h1 class="landh1">Мир математики</h1>
                                <h2 class="landh2">Урок математики слишком легкий для тебя? <br> Ты пришел в нужное место! </h2>

                                <img class="col-md-6 imgland" src="<?=base_url("images/3.jpeg")?>" alt="">

                                
                                <p>Удивляйся красоте <br> Скачивай статьи и книги <br> Поделись своими идеями с тысячами любителей со всего мира</p>
                                <a href="#" class="btn btn-common btn-lg">НАЧАТЬ СЕЙЧАС</a>
                                
                        

                </div>
            
            
                
                
    

    <!-- Featured -->
        <div id="featured">
            <div class="container">
                
                    <h2>Community</h2>
            


                <p>Встретил задачу в книге / на олимпиаде / на доске, решал час, два, неделю. И ты знаешь, что другой человек (победитель) смог решить эту задачу. Ты тоже хочешь побеждать, но потратил час или неделю на задачу не из-за этого, а из желания узнать решение. <br><br>

В конце сдаешься и смотришь решение. Развиваешься. <br><br>

Сайт предоставляет возможность не только смотреть решения и самому развиваться, но и помогать другим, выложив свои решения, опыт и советы.</p>
                <hr />
                
    <div class="qvadr">
        <div class="content container pvertical clearfix " >
            <div class="gallery item forum">
                <div class="asset">
                    <a class="thumb" href="#">
                        <img alt="forum" src="<?=base_url("images/s3.jpg")?>" />
                    </a>
                </div>
                <div class="info">
                   <h1>
                        <a href="#" >Bookstore</a>
                    </h1>
                </div>
                
            </div>
        </div>

        <div class="content container pvertical clearfix">
            <div class="gallery item bookstore">
                
                <div class="asset">
                    <a class="thumb" href="#">
                        <img alt="forum" src="<?=base_url("images/s4.jpg")?>" />
                    </a>
                </div>
                <div class="info">
                    <h1>
                        <a href="#" >Bookstore</a>
                    </h1>
                </div>
            </div>
        </div>

        <div class="content container pvertical clearfix">
            <div class="gallery item resources">
                
                <div class="asset">
                    <a href="#">
                        <img alt="forum" src="<?=base_url("images/s5.jpg")?>" />
                    </a>
                </div>
                <div class="info">
                    <h1>
                        <a href="#" >Resources</a>
                    </h1>
                </div>
            </div>
        </div>

        <div class="content container pvertical clearfix">
            <div class="gallery item s">
                
                <div class="asset">
                    <a class="thumb" href="#">
                        <img alt="forum" src="<?=base_url("images/s3.jpg")?>" />
                    </a>
                </div>
                <div class="info">
                    <h1>
                        <a href="#" class="pverticaltext">Page 1</a>
                    </h1>
                </div>
            </div>
        </div>
       
        <div class="content container pvertical clearfix">
            <div class="gallery item q5">
                
                <div class="asset">
                    <a href="#">
                        <img alt="forum" src="<?=base_url("images/s3.jpg")?>" />
                    </a>
                </div>
                <div class="info">
                    <h1>
                        <a href="#" class="pverticaltext">Forum</a>
                    </h1>
                </div>

            </div>
        </div>


        <div class="content container pvertical clearfix">
            <div class="gallery item q6">
                
                <div class="asset">
                    <a class="thumb" href="#">
                        <img alt="forum" src="<?=base_url("images/s3.jpg")?>" />
                    </a>
                </div>
                <div class="info">
                    <h1>
                        <a href="#" class="pverticaltext">Bookstore</a>
                    </h1>
                </div>
            </div>
        </div>

        <div class="content container pvertical clearfix">
            <div class="gallery item q7">
                
                <div class="asset">
                    <a class="thumb" href="#">
                        <img alt="forum" src="<?=base_url("images/s3.jpg")?>" />
                    </a>
                </div>
                <div class="info">
                    <h1>
                        <a href="#" class="pverticaltext">Resources</a>
                    </h1>
                </div>
            </div>
        </div>

        <div class="content container pvertical clearfix">
            <div class="gallery item q8">
                
                <div class="asset">
                    <a class="thumb" href="#">
                        <img alt="forum" src="<?=base_url("images/s3.jpg")?>" />
                    </a>
                </div>
                <div class="info">
                    <h1>
                        <a href="#">S</a>
                    </h1>
                </div>
            </div>
        </div>
    </div>
            </div>
        </div>

    <!-- Main -->
        <section id="contact">
    <div class="container text-center">
    <div class="row">
    <h1 class="title">Свяжитесь с нами</h1>

    <h2 class="subtitle">Запишитись на ежемесячну рассылку</h2>


    <form role="form" class="contact-form" method="post">
    <div class="col-md-6 wow fadeInLeft" data-wow-delay=".5s">
    <div class="form-group">
    <div class="controls">
    <input type="text" class="form-control" placeholder="Name" name="name">
    </div>
    </div>
    <div class="form-group">
    <div class="controls">
    <input type="email" class="form-control email" placeholder="Email" name="email">
    </div>
    </div>
    <div class="form-group">
    <div class="controls">
    <input type="text" class="form-control requiredField" placeholder="Subject" name="subject">
    </div>
    </div>

    <div class="form-group">

    <div class="controls">
    <textarea rows="7" class="form-control" placeholder="Message" name="message"></textarea>
    </div>
    </div>
    <button type="submit" id="submit" class="btn btn-lg btn-common">Отправить</button><div id="success" style="color:#34495e;"></div>

    </div>
    </form>

    <div class="col-md-6 wow fadeInRight">
    
<div class="contact-info">
    <p> Алматы, Казахстан</p>
     <p> ukuanysh@mail.ru</p>
</div>

    <p>
    <br>
    </p>
    <p>Звоните нам бесплатно!</p> <br><a href="tel:+77075206002"> +7 (707) 520 60 02 </a>
    
    </div>

    </div>
    </div>
    </section>














